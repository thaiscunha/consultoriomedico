package br.com.consultorioMedico.conexaoMsql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaConexao {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {

		Connection con = new ConnectionPool().getConnection();
		Statement st = con.createStatement();
		st.executeQuery("SELECT * FROM Medico");
		ResultSet rs = st.getResultSet();

		while (rs.next()) {
			int id = rs.getInt("id");
			System.out.println(id);
			String nome = rs.getString("nome");
			System.out.println(nome);
			String crm = rs.getString("crm");
			System.out.println(crm);
		}

		rs.close();
		st.close();
		con.close();

	}

}
