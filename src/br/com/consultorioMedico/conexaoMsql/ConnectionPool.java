package br.com.consultorioMedico.conexaoMsql;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnectionPool {
	private DataSource datasource;

	public ConnectionPool() {

		MysqlDataSource pool = new MysqlDataSource();
		pool.setURL("jdbc:mysql://localhost/consultorio");
		pool.setUser("root");
		pool.setPassword("123456");
		this.datasource = pool;
	} 

	public Connection getConnection() throws SQLException {
		Connection con = datasource.getConnection();
		return con;

	}

	public DataSource getDatasource() {
		return datasource;
	}
	
	
}
