package br.com.consutorioMedico.exception;

import java.io.Serializable;

public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String key;

	public Message(String key) {
		super();
		this.key = key;
	}

	public Message(Integer id, String key) {
		super();
		this.id = id;
		this.key = key;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
