package br.com.consutorioMedico.exception;


public class DBException extends Exception {
	private static final long serialVersioUID = 1L;
	private Message msg;

	public DBException(Throwable t) {
		super(t);
		
	}	

	public DBException(Message msg) {
		super();
		this.setMsg(msg);
		
	}

	public DBException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public Message getMsg() {
		return msg;
	}

	public void setMsg(Message msg) {
		this.msg = msg;
	}

	

	

}
