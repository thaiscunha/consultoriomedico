package br.com.consutorioMedico.modelo;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class Teste {
	public static void main(String[] args) {
		HistoricoMedico historicoMedico = new HistoricoMedico();
		historicoMedico.setId(1l);
		historicoMedico.setResultadoUltimosExames("Canal co nervo periodontal inflamado");
		historicoMedico.setMedicamento("Atibi�tico Clavulin");
		historicoMedico.setTipoDeDoenca("Sensibilidade dos dentes");

		Endereco enderecoPaciente = new Endereco();
		enderecoPaciente.setId(1l);
		enderecoPaciente.setLogradouro("Rua Domingos dos Santos");
		enderecoPaciente.setBairro("bento Ribeiro");
		enderecoPaciente.setCep("21550580");
		enderecoPaciente.setComplemento("Sobrado");
		enderecoPaciente.setEstado(" Rio de Janeiro");

		Medico medico = new Medico();
		medico.setId(1l);
		medico.setNome("Dr Jo�o");
		medico.setCrm("22.333");
		medico.setEspecialidade("Endodentista");

		Paciente paciente = new Paciente();
		paciente.setId(1l);
		paciente.setNome("Thais");
		paciente.setCpf("10958506795");
		paciente.setSexo("feminino");
		paciente.setTelefone("987335582");
		paciente.setDataNascimento(LocalDate.of(1986, Month.MAY, 20));
		paciente.setEndereco(enderecoPaciente);
		paciente.setHistoricoMedico(historicoMedico);

		Consulta consulta = new Consulta();
		consulta.setId(1l);
		consulta.setDataConsulta(LocalDate.of(2015, Month.JULY, 03));
		consulta.setInformaoes("Foi realizado procedimento cl�nico de raspagem na gengiva.");
		consulta.setMedico(medico);
		consulta.setPaciente(paciente);
		consulta.setAtendido(true);

		Consulta consulta2 = new Consulta();
		consulta2.setId(10l);
		consulta2.setDataConsulta(LocalDate.of(2015, Month.AUGUST, 03));
		consulta2.setInformaoes("Foi realizada extra��o do dente.");
		consulta2.setMedico(medico);
		consulta2.setPaciente(paciente);
		consulta2.setAtendido(true);

		List<Consulta> consultas = new ArrayList<Consulta>();
		consultas.add(consulta);
		consultas.add(consulta2);

		paciente.setConsultas(consultas);
		medico.setConsultas(consultas);

		System.out.println(consulta);
		System.out.println(consulta2);

	}

}
