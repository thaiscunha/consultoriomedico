package br.com.consutorioMedico.modelo;

public class HistoricoMedico {
	private Long id;
	private String medicamento;
	private String resultadoUltimosExames;
	private String tipoDeDoenca;

	private Paciente paciente;
	
	public String getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}

	public String getResultadoUltimosExames() {
		return resultadoUltimosExames;
	}

	public void setResultadoUltimosExames(String resultadoUltimosExames) {
		this.resultadoUltimosExames = resultadoUltimosExames;
	}

	public String getTipoDeDoenca() {
		return tipoDeDoenca;
	}

	public void setTipoDeDoenca(String tipoDeDoenca) {
		this.tipoDeDoenca = tipoDeDoenca;
	}
	

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	@Override
	public String toString() {
		return "HistoricoMedico [id=" + id + ", medicamento=" + medicamento + ", resultadoUltimosExames="
				+ resultadoUltimosExames + ", tipoDeDoenca=" + tipoDeDoenca + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
