package br.com.consutorioMedico.modelo;

import java.time.LocalDate;

public class Consulta {
	private Long id;
	private LocalDate dataConsulta;
	private String informaoes;
	private Boolean atendido = Boolean.FALSE;

	private Medico medico;
	private Paciente paciente;
	//eu chego no pagamento atravez da consulta e na tabela quando h� relacao 1pra1 tanto faz onde fica
	//a chave estrangeira
	private Pagamento pagamento;

	public LocalDate getDataConsulta() {
		return dataConsulta;
	}

	public void setDataConsulta(LocalDate dataConsulta) {
		this.dataConsulta = dataConsulta;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Boolean getAtendido() {
		return atendido;
	}

	public String getInformaoes() {
		return informaoes;
	}

	public void setInformaoes(String informaoes) {
		this.informaoes = informaoes;
	}

	public void setAtendido(Boolean atendido) {
		this.atendido = atendido;
	}

	@Override
	public String toString() {
		return "Consulta [id=" + id + ", dataConsulta=" + dataConsulta + ", medico=" + medico + ", paciente="
				+ paciente + ", informaoes=" + informaoes + ", atendido=" + atendido + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pagamento getPagamento() {
		return pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

}
