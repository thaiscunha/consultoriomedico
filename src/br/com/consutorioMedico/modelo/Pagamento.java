package br.com.consutorioMedico.modelo;


public class Pagamento {

	private Long id;
	private Integer codAutorizacao;
	private double valor;

	private TipoPagamento tipoPagamento;
	private Consulta consulta;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public Integer getCodAutorizacao() {
		return codAutorizacao;
	}

	public void setCodAutorizacao(Integer codAutorizacao) {
		this.codAutorizacao = codAutorizacao;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
