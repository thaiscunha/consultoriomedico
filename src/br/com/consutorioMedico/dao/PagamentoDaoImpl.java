package br.com.consutorioMedico.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.exception.Message;
import br.com.consutorioMedico.modelo.Consulta;
import br.com.consutorioMedico.modelo.Medico;
import br.com.consutorioMedico.modelo.Paciente;
import br.com.consutorioMedico.modelo.Pagamento;
import br.com.consutorioMedico.modelo.TipoPagamento;

public class PagamentoDaoImpl extends AbstractDao <Pagamento> {

	@Override
	public boolean incluir(Pagamento model) throws DBException {
		StringBuilder sql=new StringBuilder(" INSERT INTO Pagamento ");
		sql.append("( codAutorizacao, valor, Tipo_Pagamento_id, Consulta_id )")
		.append("VALUES( ?,?,?,? )");
		
		try {
			open(sql,true);
			
			int param=0;
			PreparedStatement pstmt=getPreparedStatmant();
			pstmt.setInt(++param, model.getCodAutorizacao());
			pstmt.setDouble(++param,model.getValor());
			pstmt.setLong(++param, model.getTipoPagamento().getId());
			pstmt.setLong(++param, model.getConsulta().getId());
			
			boolean executou=getPreparedStatmant().executeUpdate() > 0;
			
			setResultSet(getPreparedStatmant().getGeneratedKeys());
			if(executou && getResultSet().next()) {
				model.setId(getResultSet().getLong(1));
			}
			
			return executou;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.pagamento.incluir"));
		}finally {
			close();
		}
	}

	@Override
	public boolean alterar(Pagamento model) throws DBException {
		StringBuilder sql =new StringBuilder(" UPDATE Pagamento ");
		sql.append(" SET codAutorizacao = ?, valor = ?, Tipo_Pagamento_id = ?, Consulta_id = ?  ")
		.append(" WHERE id = ? ");
		
		try {
			open(sql);
			
			int param=0;
			PreparedStatement pstmt=getPreparedStatmant();
			pstmt.setInt(++param, model.getCodAutorizacao());
			pstmt.setDouble(++param, model.getValor());
			pstmt.setLong(++param, model.getTipoPagamento().getId());
			pstmt.setLong(++param, model.getConsulta().getId());
			pstmt.setLong(++param, model.getId());
			
			return getPreparedStatmant().executeUpdate()>0;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.pagamento.alterar"));
		} finally {
			close();
		}
	}

	@Override
	public boolean excluir(Pagamento model) throws DBException {
		StringBuilder sql=new StringBuilder(" DELETE FROM Pagamento WHERE id = ? ");
		
		try {
			
			open(sql);
			
			getPreparedStatmant().setLong(1,model.getId());
			
			return getPreparedStatmant().executeUpdate() > 0;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.pagamento.excluir"));
		}finally {
			close();
		}
	}

	@Override
	public Pagamento obter(Long id) throws DBException {
		Pagamento pagamento = null;
		StringBuilder sql=new StringBuilder(" SELECT pgt.id id_Pagamento, pgt.codAutorizacao, pgt.valor, ");
		sql.append(" tipg.id id_tipo_pagamento, tipg.valor_referencia, tipg.descricao, ")
		   .append(" con.id id_consulta, con.dataConsulta, con.informacoes, con.atendido,")
		   .append(" med.id id_medico, med.crm, med.nome, med.especialidade,")
		   .append(" pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone")
		   .append(" FROM  Pagamento pgt ")
		   .append(" INNER JOIN Tipo_Pagamento tipg ON pgt.tipo_pagamento_id = tipg.id ")
		   .append(" INNER JOIN Consulta con ON pgt.consulta_id = con.id ")
		   .append(" INNER JOIN Medico med ON con.medico_id = med.id ")
		   .append(" INNER JOIN Paciente pac ON con.paciente_id = pac.id ")
		   .append(" WHERE pgt.id = ? ");
		//INNER JOIN Paciente pac ON his.Paciente_idPaciente = pac.id
		
		try {
			open(sql);
			
			if(id==null) {
				getPreparedStatmant().setNull(1, Types.NULL);
			}else {
				getPreparedStatmant().setLong(1, id);
			}
			
			setResultSet(getPreparedStatmant().executeQuery());
			ResultSet rs = getResultSet();
			
			if(rs.next()) {
				pagamento=obterPagamento(rs);
			}
			
			return pagamento;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.pagamento.obter"));
		} finally {
			
		}
	}

	private Pagamento obterPagamento(ResultSet rs) throws SQLException {
		Paciente paciente = new Paciente();
		paciente.setId(rs.getLong("id_paciente"));
		paciente.setNome(rs.getString("nome"));
		paciente.setCpf(rs.getString("cpf"));
		paciente.setDataNascimento(rs.getDate("dataNascimento").toLocalDate());
		paciente.setSexo(rs.getString("sexo"));
		paciente.setTelefone(rs.getString("telefone"));
		
		Medico medico = new Medico();
		medico.setId(rs.getLong("id_medico"));
		medico.setCrm(rs.getString("crm"));
		medico.setNome(rs.getString("nome"));
		medico.setEspecialidade(rs.getString("especialidade"));
		
		Consulta con=new Consulta();
		con.setId(rs.getLong("id_consulta"));
		con.setInformaoes(rs.getString("informacoes"));
		con.setDataConsulta(rs.getDate("dataConsulta").toLocalDate());
		con.setAtendido(rs.getBoolean("atendido"));
		con.setMedico(medico);
		con.setPaciente(paciente);
		
		TipoPagamento tipoPagamento=new TipoPagamento();
		tipoPagamento.setId(rs.getLong("id_tipo_pagamento"));
		tipoPagamento.setDescricao(rs.getString("descricao"));
		tipoPagamento.setValorReferencia(rs.getDouble("valor_referencia"));
		
		Pagamento pagamento=new Pagamento();
		pagamento.setId(rs.getLong("id_pagamento"));
		pagamento.setCodAutorizacao(rs.getInt("codAutorizacao"));
		pagamento.setValor(rs.getDouble("valor"));
		pagamento.setTipoPagamento(tipoPagamento);
		pagamento.setConsulta(con);
		
		
		return pagamento;
	}

	@Override
	public List<Pagamento> listar(Pagamento filtro) throws DBException {
		List<Pagamento> pagamentos=new ArrayList<Pagamento>();
		StringBuilder sql=new StringBuilder();
		sql.append(" SELECT pgt.id id_Pagamento, pgt.codAutorizacao, pgt.valor, ")
		   .append(" tipg.id id_tipo_pagamento, tipg.valor_referencia, tipg.descricao, ")
		   .append(" con.id id_consulta, con.dataConsulta, con.informacoes, con.atendido,")
		   .append(" med.id id_medico, med.crm, med.nome, med.especialidade,")
		   .append(" pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone")
		   .append(" FROM  Pagamento pgt ")
		   .append(" INNER JOIN Tipo_Pagamento tipg ON pgt.tipo_pagamento_id = tipg.id ")
		   .append(" INNER JOIN Consulta con ON pgt.consulta_id = con.id ")
		   .append(" INNER JOIN Medico med ON con.medico_id = med.id ")
		   .append(" INNER JOIN Paciente pac ON con.paciente_id = pac.id ")
		   .append(" WHERE 1 = 1 ");
		
		List<Object> paramsValue = new ArrayList<Object>();
		
		if(filtro!=null) {
			if(filtro.getId()!=null) {
				sql.append(" AND pgt.id = ? ");
				paramsValue.add(filtro.getId());
			}
			if(filtro.getCodAutorizacao()!=null) {
				sql.append(" AND pgt.codAutorizacao = ? ");
				paramsValue.add(filtro.getCodAutorizacao());
			}
			if(filtro.getTipoPagamento()!=null && filtro.getTipoPagamento().getId()!=null) {
				sql.append(" AND pgt.id = ? ");
				paramsValue.add(filtro.getTipoPagamento().getId());
			}
			/*if(filtro.getValor() !=null) {
				sql.append(" AND pgt.valor = ? ");
				paramsValue.add(filtro.getValor());
			}*/
			
		}
		
		try {
			open(sql);
			
			for (int i = 0; i < paramsValue.size(); i++) {
				getPreparedStatmant().setObject(i+1, paramsValue.get(i));

			}
			
			setResultSet(getPreparedStatmant().executeQuery());
			
			ResultSet rs = getResultSet();
			
			while(getResultSet().next()) {
				pagamentos.add(obterPagamento(rs));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.pagamento.listar"));
		} finally {
			close();
		}
		
		return pagamentos;
	}

}
