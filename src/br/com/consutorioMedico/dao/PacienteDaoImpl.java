package br.com.consutorioMedico.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.exception.Message;
import br.com.consutorioMedico.modelo.Paciente;

public class PacienteDaoImpl extends AbstractDao<Paciente> {

	@Override
	public boolean incluir(Paciente model) throws DBException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder("INSERT INTO Paciente");
		sql.append(" (nome,cpf,dataNascimento,sexo,telefone) ").append(" VALUES(?,?,?,?,?) ");

		try {
			open(sql, true);

			int param = 0;

			PreparedStatement pstmt = getPreparedStatmant();
			pstmt.setString(++param, model.getNome());
			pstmt.setString(++param, model.getCpf());
			pstmt.setDate(++param, java.sql.Date.valueOf(model.getDataNascimento()));
			pstmt.setString(++param, model.getSexo());
			pstmt.setString(++param, model.getTelefone());

			boolean executou = getPreparedStatmant().executeUpdate() > 0;

			setResultSet(getPreparedStatmant().getGeneratedKeys());

			if (executou && getResultSet().next()) {
				model.setId(getResultSet().getLong(1));
			}

			return executou;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.paciente.incluir"));
		} finally {
			close();
		}

	}

	@Override
	public boolean alterar(Paciente model) throws DBException {
		StringBuilder sql = new StringBuilder(" UPDATE Paciente ");
		sql.append(" SET nome = ?, cpf = ?, dataNascimento = ?, sexo = ?, telefone = ? ")
		   .append(" WHERE id = ? ");

		try {
			open(sql);
			int param = 0;
			
			PreparedStatement pstmt = getPreparedStatmant();
			pstmt.setString(++param, model.getNome());
			pstmt.setString(++param, model.getCpf());
			pstmt.setDate(++param, java.sql.Date.valueOf(model.getDataNascimento()));
			pstmt.setString(++param, model.getSexo());
			pstmt.setString(++param, model.getTelefone());
			pstmt.setLong(++param, model.getId());

			return getPreparedStatmant().executeUpdate() > 0;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.paciente.alterar"));
		} finally {
			close();
		}
	}

	@Override
	public boolean excluir(Paciente model) throws DBException {
		String sql = " DELETE FROM Paciente WHERE id = ? ";

		try {
			open(sql);

			getPreparedStatmant().setLong(1, model.getId());

			return getPreparedStatmant().executeUpdate() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.paciente.excluir"));
		} finally {
			close();
		}
	}

	@Override
	public Paciente obter(Long id) throws DBException {
		Paciente paciente = null;
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone ")
				.append(" FROM Paciente pac ")
				.append(" WHERE pac.id = ? ");

		try {

			open(sql);
			if (id == null) {
				getPreparedStatmant().setNull(1, Types.NULL);
			} else {
				getPreparedStatmant().setLong(1, id);
			}

			setResultSet(getPreparedStatmant().executeQuery());
			ResultSet rs = getResultSet();

			if (rs.next()) {
				paciente = obterPaciente(rs);
			}

			return paciente;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.paciente.obter"));
		} finally {
			close();
		}

	}

	public static Paciente obterPaciente(ResultSet rs) throws SQLException {
		Paciente paciente = new Paciente();
		paciente.setId(rs.getLong("id_paciente"));
		paciente.setNome(rs.getString("nome"));
		paciente.setCpf(rs.getString("cpf"));
		paciente.setDataNascimento(rs.getDate("dataNascimento").toLocalDate());
		paciente.setSexo(rs.getString("sexo"));
		paciente.setTelefone(rs.getString("telefone"));

		
		return paciente;
	}

	@Override
	public List<Paciente> listar(Paciente filtro) throws DBException {
		List<Paciente> pacientes=new ArrayList<Paciente>();
		StringBuilder sql =new StringBuilder();
		sql.append(" SELECT pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone ")
		   .append(" FROM Paciente pac ")
		   .append(" WHERE 1=1 ");
		
		List<Object> paramsValue= new ArrayList<Object>();
		if(filtro!=null) {
			if(filtro.getId()!=null) {
				sql.append("AND pac.id = ?");
				paramsValue.add(filtro.getId());
			}
			if(filtro.getNome()!=null) {
				sql.append("AND pac.nome = ?");
				paramsValue.add(filtro.getNome());
			}
			if(filtro.getCpf()!=null) {
				sql.append("AND pac.cpf = ?");
				paramsValue.add(filtro.getCpf());
			}
			if(filtro.getDataNascimento()!=null) {
				sql.append("AND pac.dataNascimento = ?");
				paramsValue.add(filtro.getDataNascimento());
			}
			if(filtro.getSexo()!=null) {
				sql.append("AND pac.sexo = ?");
				paramsValue.add(filtro.getSexo());
			}
			if(filtro.getTelefone()!=null) {
				sql.append("AND pac.telefone = ?");
				paramsValue.add(filtro.getTelefone());
			}
		}
		
		try {
			open(sql);
			for (int i = 0; i < paramsValue.size(); i++) {
				getPreparedStatmant().setObject(i+1, paramsValue.get(i));
			}
			setResultSet(getPreparedStatmant().executeQuery());
			
			ResultSet rs = getResultSet();
			
			while(rs.next()) {
				pacientes.add(obterPaciente(rs));
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.paciente.listar "));
		} finally {
			close();
		}
		return pacientes;
	}

}
