
package br.com.consutorioMedico.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.exception.Message;
import br.com.consutorioMedico.modelo.HistoricoMedico;
import br.com.consutorioMedico.modelo.Paciente;

public class HistoricoMedicoDaoImpl extends AbstractDao<HistoricoMedico> {

	@Override
	public boolean incluir(HistoricoMedico model) throws DBException {
		StringBuilder sql = new StringBuilder("INSERT INTO Historico_Medico");
		sql.append(" (medicamento, resultadoUltimosExames, tipoDeDoenca, Paciente_idPaciente) ")
		   .append(" VALUES(?,?,?,?) ");

		try {
			open(sql, true);

			int param = 0;

			PreparedStatement pstmt = getPreparedStatmant();
			pstmt.setString(++param, model.getMedicamento());
			pstmt.setString(++param, model.getResultadoUltimosExames());
			pstmt.setString(++param, model.getTipoDeDoenca());
			pstmt.setLong(++param, model.getPaciente().getId());

			boolean executou = getPreparedStatmant().executeUpdate() > 0;

			setResultSet(getPreparedStatmant().getGeneratedKeys());

			if (executou && getResultSet().next()) {
				model.setId(getResultSet().getLong(1));
			}

			return executou;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.historico_medico.incluir"));
		} finally {
			close();
		}
	}

	@Override
	public boolean alterar(HistoricoMedico model) throws DBException {
		StringBuilder sql = new StringBuilder(" UPDATE Historico_Medico ");
		sql.append(" SET medicamento = ?, resultadoUltimosExames = ?, tipoDeDoenca = ?, Paciente_idPaciente = ? ")
		   .append(" WHERE id = ? ");

		try {
			open(sql);

			int param = 0;

			PreparedStatement pstmt = getPreparedStatmant();
			pstmt.setString(++param, model.getMedicamento());
			pstmt.setString(++param, model.getResultadoUltimosExames());
			pstmt.setString(++param, model.getTipoDeDoenca());
			pstmt.setLong(++param, model.getPaciente().getId());
			pstmt.setLong(++param, model.getId());

			return getPreparedStatmant().executeUpdate() > 0;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.historico_medico.alterar"));
		} finally {
			close();
		}
	}

	@Override
	public boolean excluir(HistoricoMedico model) throws DBException {
		StringBuilder sql = new StringBuilder(" DELETE FROM Historico_Medico WHERE id = ? ");

		try {
			open(sql);

			getPreparedStatmant().setLong(1, model.getId());

			return getPreparedStatmant().executeUpdate() > 0;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.historico_medico.excluir"));
		} finally {
			close();
		}
	}

	@Override
	public HistoricoMedico obter(Long id) throws DBException {
		HistoricoMedico historicoMedico = null;
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT his.id id_historico_Medico, his.medicamento, his.resultadoUltimosExames, his.tipoDeDoenca, ")
		   .append(" pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone ")
		   .append(" FROM Historico_Medico his ")
		   .append(" INNER JOIN Paciente pac ON his.Paciente_idPaciente = pac.id ")
		   .append(" WHERE his.id = ? ");

		try {
			open(sql);

			if (id == null) {
				getPreparedStatmant().setNull(1, Types.NULL);
			} else {
				getPreparedStatmant().setLong(1, id);
			}

			setResultSet(getPreparedStatmant().executeQuery());
			ResultSet rs = getResultSet();

			if (rs.next()) {
				historicoMedico = obterHistoricoMedico(rs);
			}

			return historicoMedico;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.historico_medico.obter"));
		} finally {
			close();
		}
	}

	private HistoricoMedico obterHistoricoMedico(ResultSet rs) throws SQLException {

		Paciente pac = new Paciente();
		pac.setId(rs.getLong("id_paciente"));
		pac.setNome(rs.getString("nome"));
		pac.setCpf(rs.getString("cpf"));
		pac.setDataNascimento(rs.getDate("dataNascimento").toLocalDate());
		pac.setSexo(rs.getString("sexo"));
		pac.setTelefone(rs.getString("telefone"));

		HistoricoMedico historico_Medico = new HistoricoMedico();
		historico_Medico.setId(rs.getLong("id_historico_Medico"));
		historico_Medico.setMedicamento(rs.getString("medicamento"));
		historico_Medico.setResultadoUltimosExames(rs.getString("resultadoUltimosExames"));
		historico_Medico.setTipoDeDoenca(rs.getString("tipoDeDoenca"));
		historico_Medico.setPaciente(pac);

		return historico_Medico;
	}

	@Override
	public List<HistoricoMedico> listar(HistoricoMedico filtro) throws DBException {
		List<HistoricoMedico> historicos = new ArrayList<>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT his.id id_historico_Medico, his.medicamento, his.resultadoUltimosExames, his.tipoDeDoenca, ")
		   .append(" pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone ")
		   .append(" FROM Historico_Medico his ")
		   .append(" INNER JOIN Paciente pac ON his.Paciente_idPaciente = pac.id ")
		   .append(" WHERE 1 = 1 ");

		List<Object> paramsValue = new ArrayList<Object>();

		if (filtro != null) {
			if (filtro.getId() != null) {
				sql.append(" AND his.id = ? ");
				paramsValue.add(filtro.getId());
			}
			if (filtro.getMedicamento() != null) {
				sql.append(" AND his.medicamento = ? ");
				paramsValue.add(filtro.getMedicamento());
			}
			if (filtro.getPaciente() != null && filtro.getPaciente().getId() != null) {
				sql.append(" AND pac.id = ? ");
				paramsValue.add(filtro.getPaciente().getId());
			}
			if (filtro.getResultadoUltimosExames() != null) {
				sql.append(" AND his.resultadoUltimosExames = ? ");
				paramsValue.add(filtro.getResultadoUltimosExames());
			}
			if (filtro.getTipoDeDoenca() != null) {
				sql.append(" AND his.tipoDeDoenca = ? ");
				paramsValue.add(filtro.getTipoDeDoenca());
			}
		}
		
		try {
			open(sql);

			for (int i = 0; i < paramsValue.size(); i++) {
				getPreparedStatmant().setObject(i + 1, paramsValue.get(i));

			}

			setResultSet(getPreparedStatmant().executeQuery());

			ResultSet rs = getResultSet();

			while (getResultSet().next()) {
				historicos.add(obterHistoricoMedico(rs));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.historico_medico.listar"));
		} finally {
			close();
		}

		return historicos;

	}
}
