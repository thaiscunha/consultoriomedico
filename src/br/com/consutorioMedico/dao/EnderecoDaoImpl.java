package br.com.consutorioMedico.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.exception.Message;
import br.com.consutorioMedico.modelo.Endereco;
import br.com.consutorioMedico.modelo.Paciente;

public class EnderecoDaoImpl extends AbstractDao<Endereco> {

	@Override
	public boolean incluir(Endereco model) throws DBException {
		StringBuilder sql = new StringBuilder("INSERT INTO Endereco");
		sql.append(" ( logradouro, bairro, estado, complemento, cep, Paciente_idPaciente ) ")
		   .append(" VALUES(?, ?, ?, ?, ?, ?) ");

		try {
			open(sql, true);

			int param = 0;

			PreparedStatement pstmt = getPreparedStatmant();

			pstmt.setString(++param, model.getLogradouro());
			pstmt.setString(++param, model.getBairro());
			pstmt.setString(++param, model.getEstado());
			pstmt.setString(++param, model.getComplemento());
			pstmt.setString(++param, model.getCep());
			pstmt.setLong(++param, model.getPaciente().getId());

			boolean executou = getPreparedStatmant().executeUpdate() > 0;

			setResultSet(getPreparedStatmant().getGeneratedKeys());

			if (executou && getResultSet().next()) {
				model.setId(getResultSet().getLong(1));
			}

			return executou;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.endereco.incluir"));
		} finally {
			close();
		}
		
	}

	@Override
	public boolean alterar(Endereco model) throws DBException {
		StringBuilder sql =new StringBuilder(" UPDATE Endereco ");
		sql.append(" SET logradouro = ? , bairro = ?, estado = ?, complemento = ?, cep = ?, Paciente_idPaciente = ? ")
		   .append(" WHERE id = ?");
		
		try {
		open(sql);
		
		int param=0;
		PreparedStatement pstmt=getPreparedStatmant();
		pstmt.setString(++param,model.getLogradouro());
		pstmt.setString(++param,model.getBairro());
		pstmt.setString(++param,model.getEstado());
		pstmt.setString(++param,model.getComplemento());
		pstmt.setString(++param,model.getCep());
		pstmt.setLong(++param,model.getPaciente().getId());
		pstmt.setLong(++param, model.getId());
		
		return getPreparedStatmant().executeUpdate() > 0;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.endereco.alterar"));
		} finally {
			close();
		}
		
	}

	@Override
	public boolean excluir(Endereco model) throws DBException {
		StringBuilder sql = new StringBuilder(" DELETE FROM Endereco WHERE id = ? ");
		try {
		open(sql);
		getPreparedStatmant().setLong(1,model.getId());
		
		return getPreparedStatmant().executeUpdate() > 0;
		
			
		} catch (Exception e) {
			throw new DBException(new Message("db.erro.endereco.excluir"));
		}finally {
			close();
		}
	}

	@Override
	public Endereco obter(Long id) throws DBException {
		Endereco endereco = null;
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT  e.id id_endereco, e.logradouro, e.bairro, e.estado, e.complemento, e.cep, ")
			.append(" pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone ")
		   .append(" FROM Endereco e ")
		   .append(" INNER JOIN Paciente pac ON e.Paciente_idPaciente = pac.id ")
		   .append(" WHERE e.id = ? ");
		
		try {
		open(sql);
		
		if(id==null) {
			getPreparedStatmant().setNull(1, Types.NULL);
		}else {
			getPreparedStatmant().setLong(1, id);
		}
		
		setResultSet(getPreparedStatmant().executeQuery());	
		ResultSet rs=getResultSet();
		
		if(rs.next()) {
			endereco=obterEndereco(rs);
		}
		
		return endereco;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.endereco.obter"));
		}  finally {
			close();
		}
		

}

	private Endereco obterEndereco(ResultSet rs) throws SQLException{
		Paciente paciente = new Paciente();
		paciente.setId(rs.getLong("id_paciente"));
		paciente.setNome(rs.getString("nome"));
		paciente.setCpf(rs.getString("cpf"));
		paciente.setDataNascimento(rs.getDate("dataNascimento").toLocalDate());
		paciente.setSexo(rs.getString("sexo"));
		paciente.setTelefone(rs.getString("telefone"));
		
		Endereco endereco = new Endereco();
		endereco.setId(rs.getLong("id_endereco"));
		endereco.setLogradouro(rs.getString("logradouro"));
		endereco.setBairro(rs.getString("bairro"));
		endereco.setCep(rs.getString("cep"));
		endereco.setEstado(rs.getString("estado"));
		endereco.setComplemento(rs.getString("complemento"));
		endereco.setPaciente(paciente);
		
		return endereco;
	}

	@Override
	public List<Endereco> listar(Endereco filtro) throws DBException {
		List<Endereco> enderecos = new ArrayList<>();
		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT  e.id id_endereco, e.logradouro, e.bairro, e.estado, e.complemento, e.cep, ")
		   .append(" pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone ")
		   .append(" FROM Endereco e ")
		   .append(" INNER JOIN Paciente pac ON e.Paciente_idPaciente = pac.id ")
		   .append(" WHERE 1 = 1 ");

		List<Object> paramsValue = new ArrayList<Object>();

		if (filtro != null) {
			if (filtro.getId() != null) {
				sql.append(" AND e.id = ? ");
				paramsValue.add(filtro.getId());
			}
			if (filtro.getLogradouro() != null) {
				sql.append(" AND e.logradouro = ? ");
				paramsValue.add(filtro.getLogradouro());
			}
			if (filtro.getPaciente() != null && filtro.getPaciente().getId() != null) {
				sql.append(" AND pac.id = ? ");
				paramsValue.add(filtro.getPaciente().getId());
			}
			if (filtro.getBairro() != null) {
				sql.append(" AND e.bairro = ? ");
				paramsValue.add(filtro.getBairro());
			}
			if (filtro.getComplemento() != null) {
				sql.append(" AND e.complemento = ? ");
				paramsValue.add(filtro.getComplemento());
			}
			if (filtro.getEstado() != null) {
				sql.append(" AND e.estado = ? ");
				paramsValue.add(filtro.getEstado());
			}
			if (filtro.getCep() != null) {
				sql.append(" AND e.cep = ? ");
				paramsValue.add(filtro.getCep());
			}
		}
		
		try {
			open(sql);

			for (int i = 0; i < paramsValue.size(); i++) {
				getPreparedStatmant().setObject(i + 1, paramsValue.get(i));

			}

			setResultSet(getPreparedStatmant().executeQuery());

			ResultSet rs = getResultSet();

			while (getResultSet().next()) {
				enderecos.add(obterEndereco(rs));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.endereco.listar"));
		} finally {
			close();
		}

		return enderecos;
	}

}
