package br.com.consutorioMedico.dao;

import java.util.List;

import br.com.consutorioMedico.exception.DBException;

public interface DAO<T> {
	// o jef colocou o throws DBException,mas n�o sei devo 
	boolean incluir(T model) throws DBException;
	boolean alterar(T model) throws DBException;
	boolean excluir(T model) throws DBException;
	T obter(Long id) throws DBException;
	List<T> listar (T filtro) throws DBException;
	

}
