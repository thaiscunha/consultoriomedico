package br.com.consutorioMedico.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.exception.Message;
import br.com.consutorioMedico.modelo.Medico;

public class MedicoDaoImpl extends AbstractDao<Medico> {

	@Override
	public boolean incluir(Medico model) throws DBException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder("INSERT INTO Medico");
		sql.append(" (crm,nome,especialidade) ").append(" VALUES(?,?,?) ");

		try {
			open(sql, true);

			int param = 0;

			PreparedStatement pstmt = getPreparedStatmant();
			pstmt.setString(++param, model.getCrm());
			pstmt.setString(++param, model.getNome());
			pstmt.setString(++param, model.getEspecialidade());

			boolean executou = getPreparedStatmant().executeUpdate() > 0;

			setResultSet(getPreparedStatmant().getGeneratedKeys());

			if (executou && getResultSet().next()) {
				model.setId(getResultSet().getLong(1));
			}

			return executou;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.medico.incluir"));
		} finally {
			close();
		}

	}

	@Override
	public boolean alterar(Medico model) throws DBException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder("UPDATE Medico");
		sql.append(" SET crm = ?, nome = ?, especialidade = ? ").append(" WHERE id=? ");

		try {
			open(sql);

			int param = 0;
			PreparedStatement pstmt = getPreparedStatmant();
			pstmt.setString(++param, model.getCrm());
			pstmt.setString(++param, model.getNome());
			pstmt.setString(++param, model.getEspecialidade());
			pstmt.setLong(++param, model.getId());

			return getPreparedStatmant().executeUpdate() > 0;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.medico.alterar"));
		} finally {
			close();
		}

	}

	@Override
	public boolean excluir(Medico model) throws DBException {
		String sql = "DELETE FROM Medico WHERE id = ? ";

		try {
			open(sql);

			getPreparedStatmant().setLong(1, model.getId());

			return getPreparedStatmant().executeUpdate() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.medico.excluir"));
		} finally {
			close();
		}

	}

	@Override
	public Medico obter(Long id) throws DBException {
		Medico medico = null;
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT med.id id_medico, med.crm, med.nome, med.especialidade FROM Medico med")
		   .append(" WHERE med.id=? ");

		try {
			open(sql);
			if (id == null) {
				getPreparedStatmant().setNull(1, Types.NULL);
			} else {
				getPreparedStatmant().setLong(1, id);
			}

			setResultSet(getPreparedStatmant().executeQuery());
			ResultSet rs = getResultSet();

			if (rs.next()) {
				medico = obterMedico(rs);
			}
			return medico;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.medico.obter"));
		} finally {
			close();
		}
	}

	@Override
	public List<Medico> listar(Medico filtro) throws DBException {
		List<Medico> medicos = new ArrayList<Medico>();
		StringBuilder sql=new StringBuilder();
		sql.append(" SELECT med.id id_medico, med.crm, med.nome, med.especialidade ")
		   .append(" FROM Medico med ")
		   .append(" WHERE 1 = 1 ");
		
		List<Object> paramsvalue =new ArrayList<Object>();
		
		//Crit�rio de busca no Banco
		if(filtro!=null) {
			if(filtro.getId()!=null) {
				sql.append(" AND med.id = ? ");
				paramsvalue.add(filtro.getId());
			}
			if(filtro.getCrm()!=null) {
				sql.append(" AND med.crm = ? ");
				paramsvalue.add(filtro.getCrm());
			}
			if(filtro.getNome()!=null) {
				sql.append(" AND med.nome = ? ");
				paramsvalue.add(filtro.getNome());
			}
			if(filtro.getEspecialidade()!=null) {
				sql.append(" AND med.especialidade = ? ");
				paramsvalue.add(filtro.getEspecialidade());
			}
		}
		
		try {
			open(sql);
			for(int i=0;i<paramsvalue.size();i++) {
				getPreparedStatmant().setObject(i+1, paramsvalue.get(i));
			}
			setResultSet(getPreparedStatmant().executeQuery());
			
			ResultSet rs= getResultSet();
			
			while(getResultSet().next()) {
				medicos.add(obterMedico(rs));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.medico.listar"));
		}finally {
			close();
		}
		
		return medicos;
	}

	private Medico obterMedico(ResultSet rs) throws SQLException {
		Medico medico = new Medico();
		medico.setId(rs.getLong("id_medico"));
		medico.setCrm(rs.getString("crm"));
		medico.setNome(rs.getString("nome"));
		medico.setEspecialidade(rs.getString("especialidade"));

		return medico;
	}
}
