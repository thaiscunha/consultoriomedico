package br.com.consutorioMedico.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.exception.Message;
import br.com.consutorioMedico.modelo.TipoPagamento;

public class TipoPagamentoDaoImpl extends AbstractDao<TipoPagamento>{

	@Override
	public boolean incluir(TipoPagamento model) throws DBException {
		StringBuilder sql=new StringBuilder("INSERT INTO Tipo_Pagamento ");
		sql.append(" (valor_referencia, descricao) ")
		.append("VALUES(?,?)");
		
		try {
		  open(sql,true);
			int param=0;
			
			PreparedStatement pstmt=getPreparedStatmant();
			pstmt.setDouble(++param,model.getValorReferencia());
			pstmt.setString(++param,model.getDescricao());
			
			boolean executou=getPreparedStatmant().executeUpdate() > 0;
			
			setResultSet(getPreparedStatmant().getGeneratedKeys());
			
			if(executou && getResultSet().next()) {
				model.setId(getResultSet().getLong(1));
			}
			
			return executou;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.tipo_pagamento.incluir"));
		}finally {
			close();
		}
	}

	@Override
	public boolean alterar(TipoPagamento model) throws DBException {
		StringBuilder sql =new StringBuilder(" UPDATE Tipo_Pagamento ");
		sql.append(" SET valor_referencia = ?, descricao = ? ")
		.append(" WHERE id = ? ");
		
		try {
			open(sql);
			
			int param=0;
			PreparedStatement pstmt=getPreparedStatmant();
			pstmt.setDouble(++param, model.getValorReferencia());
			pstmt.setString(++param, model.getDescricao());
			pstmt.setLong(++param, model.getId() );
			
			return getPreparedStatmant().executeUpdate()>0;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.tipo_pagamento.alterar"));
		} finally {
			close();
		}
		
	}

	@Override
	public boolean excluir(TipoPagamento model) throws DBException {
		StringBuilder sql=new StringBuilder(" DELETE FROM Tipo_Pagamento WHERE id = ? ");
		
		try {
			
			open(sql);
			
			getPreparedStatmant().setLong(1,model.getId());
			
			return getPreparedStatmant().executeUpdate() > 0;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.tipo_pagamento.excluir"));
		}finally {
			close();
		}
	}

	@Override
	public TipoPagamento obter(Long id) throws DBException {
		TipoPagamento tipoPagamento=null;
		StringBuilder sql=new StringBuilder(" SELECT tipg.id id_tipo_pagamento, tipg.valor_referencia, tipg.descricao ");
		sql.append(" FROM  Tipo_Pagamento tipg ")
		   .append(" WHERE tipg.id = ? ");
		
		try {
			open(sql);
			
			if(id==null) {
				getPreparedStatmant().setNull(1, Types.NULL);
			}else {
				getPreparedStatmant().setLong(1, id);
			}
			
			setResultSet(getPreparedStatmant().executeQuery());
			ResultSet rs = getResultSet();
			
			if(rs.next()) {
				tipoPagamento=obterTipoPagamento(rs);
			}
			
			return tipoPagamento;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.tipo_pagamento.obter"));
		} finally {
			
		}
	}

	private TipoPagamento obterTipoPagamento(ResultSet rs) throws SQLException {
		
		TipoPagamento tipoPagamento=new TipoPagamento();
		tipoPagamento.setId(rs.getLong("id_tipo_pagamento"));
		tipoPagamento.setDescricao(rs.getString("descricao"));
		tipoPagamento.setValorReferencia(rs.getDouble("valor_referencia"));
		return tipoPagamento;
	}

	@Override
	public List<TipoPagamento> listar(TipoPagamento filtro) throws DBException {
		List<TipoPagamento> tipoPagamentos=new ArrayList<TipoPagamento>();
		StringBuilder sql=new StringBuilder(" SELECT tipg.id id_tipo_pagamento, tipg.valor_referencia, tipg.descricao ");
		sql.append(" FROM  Tipo_Pagamento tipg ")
		   .append(" WHERE 1 = 1  ");
		
		List<Object> paramsValue=new ArrayList<Object>();
		
		if(filtro!=null) {
			if (filtro.getId() != null) {
				sql.append(" AND tipg.id = ? ");
				paramsValue.add(filtro.getId());
			}
			if (filtro.getDescricao() != null) {
				sql.append(" AND tipg.descricao = ? ");
				paramsValue.add(filtro.getDescricao());
			}
			if (filtro.getValorReferencia() != null) {
				sql.append(" AND  tipg.valor_referencia = ? ");
				paramsValue.add(filtro.getValorReferencia());
			}
			
			
		}
		
		try {
			open(sql);

			for (int i = 0; i < paramsValue.size(); i++) {
				getPreparedStatmant().setObject(i + 1, paramsValue.get(i));

			}

			setResultSet(getPreparedStatmant().executeQuery());

			ResultSet rs = getResultSet();

			while (getResultSet().next()) {
				tipoPagamentos.add(obterTipoPagamento(rs));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.tipo_pagamento.listar"));
		} finally {
			close();
		}

		return tipoPagamentos;
	}
        
}


