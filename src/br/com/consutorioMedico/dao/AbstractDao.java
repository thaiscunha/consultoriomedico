package br.com.consutorioMedico.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.sql.DataSource;

import br.com.consultorioMedico.conexaoMsql.ConnectionPool;
import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.exception.Message;

import com.mysql.jdbc.Statement;

public abstract class AbstractDao<T> implements DAO<T> {
	private DataSource ds;
	private Connection conn;
	private PreparedStatement preparedStatmant = null;
	private ResultSet resultSet = null;

	private void getDataSource() throws SQLException, NamingException {
		ConnectionPool connectionPool = new ConnectionPool();
		ds = connectionPool.getDatasource();
		
		// n�o era assim q eu queria ter feito,mas tentei outras coisas e n�o
		// consegui
		/*
		 * if(connectionPool==null) { Context ctx=new InitialContext();
		 * ds=(DataSource)ctx.lookup("java:comp/env/jdbc/DBThais");
		 * 
		 * }
		 */
	}

	public Connection open(String sql) throws DBException {
		return open(sql, false);
	}

	public Connection open(StringBuilder sql) throws DBException {
		return open(sql, false);
	}

	public Connection open(StringBuilder sql, boolean autoIncrement) throws DBException {
		return open(sql.toString(), autoIncrement);
	}

	public Connection open(String sql, boolean autoIncrement) throws DBException {
		try {
			getDataSource();
			// aqui eu gostaria q o atributo ds recebesse quem abre a conexao
			// para verificar se est� null

			if (ds == null)
				throw new DBException(new Message("erro.db.datasource"));
			
			conn = ds.getConnection();

			if (conn == null)
				throw new DBException(new Message("erro.db.datasource"));

			// Prepara e query para execucao
			if (autoIncrement) {
				setPreparedStatmant(conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS));
			} else {
				setPreparedStatmant(conn.prepareStatement(sql));
			}

		} catch (Exception e) {
			throw new DBException(e);
		}

		return conn;
	}

	public void close() {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		if (preparedStatmant != null) {
			try {
				preparedStatmant.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		if (conn != null) {
			try {
				this.conn.close();
				this.conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	public PreparedStatement getPreparedStatmant() {
		return preparedStatmant;
	}

	public void setPreparedStatmant(PreparedStatement preparedStatmant) {
		this.preparedStatmant = preparedStatmant;
	}

	public ResultSet getResultSet() {
		return resultSet;
	}

	public void setResultSet(ResultSet resultSet) {
		this.resultSet = resultSet;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

}
