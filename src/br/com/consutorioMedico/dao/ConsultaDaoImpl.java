package br.com.consutorioMedico.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.exception.Message;
import br.com.consutorioMedico.modelo.Consulta;
import br.com.consutorioMedico.modelo.Medico;
import br.com.consutorioMedico.modelo.Paciente;

public class ConsultaDaoImpl extends AbstractDao<Consulta>{

	@Override
	public boolean incluir(Consulta model) throws DBException {
		StringBuilder sql=new StringBuilder(" INSERT INTO Consulta ");
		sql.append("( dataConsulta, informacoes, atendido, Medico_id, Paciente_Id )")
		.append("VALUES(?,?,?,?,?)");
		
		try {
			open(sql,true);
			int param=0;
			
			PreparedStatement pstmt=getPreparedStatmant();
			pstmt.setDate(++param, java.sql.Date.valueOf(model.getDataConsulta()));
			pstmt.setString(++param, model.getInformaoes());
			pstmt.setBoolean(++param, model.getAtendido());
			pstmt.setLong(++param,model.getMedico().getId());
			pstmt.setLong(++param,model.getPaciente().getId());
			
			
			boolean executou=getPreparedStatmant().executeUpdate()>0;
			setResultSet(getPreparedStatmant().getGeneratedKeys());
			
			if(executou && getResultSet().next()) {
				model.setId(getResultSet().getLong(1));
			}
			
			return executou;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.consulta.incluir"));
		}finally {
			close();
		}
	}

	@Override
	public boolean alterar(Consulta model) throws DBException {
		StringBuilder sql =new StringBuilder(" UPDATE Consulta ");
		sql.append(" SET dataConsulta = ?, informacoes = ?, atendido = ?, Medico_id = ?, Paciente_id = ?  ")
		.append(" WHERE id = ? ");
		
		try {
			open(sql);
			
			int param=0;
			PreparedStatement pstmt = getPreparedStatmant();
			pstmt.setDate(++param,java.sql.Date.valueOf(model.getDataConsulta()));
			pstmt.setString(++param, model.getInformaoes());
			pstmt.setBoolean(++param, model.getAtendido());
			pstmt.setLong(++param, model.getMedico().getId());
			pstmt.setLong(++param, model.getPaciente().getId());
			pstmt.setLong(++param, model.getId());
			
			return getPreparedStatmant().executeUpdate()>0;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.consulta.alterar"));
		} finally {
			close();
		}
	}

	@Override
	public boolean excluir(Consulta model) throws DBException {
		StringBuilder sql=new StringBuilder(" DELETE FROM Consulta WHERE id = ? ");
		
		try {
			
			open(sql);
			
			getPreparedStatmant().setLong(1,model.getId());
			
			return getPreparedStatmant().executeUpdate() > 0;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.consulta.excluir"));
		}finally {
			close();
		}
	}

	@Override
	public Consulta obter(Long id) throws DBException {
		Consulta consulta = null;
		StringBuilder sql=new StringBuilder(" SELECT con.id id_consulta, con.dataConsulta, con.informacoes, con.atendido,");
		sql.append(" med.id id_medico, med.crm, med.nome, med.especialidade,")
		   .append(" pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone")
		   .append(" FROM  Consulta con ")
		   .append(" INNER JOIN Medico med ON con.medico_id = med.id ")
		   .append(" INNER JOIN Paciente pac ON con.paciente_id = pac.id ")
		   .append(" WHERE con.id = ? ");
		
		try {
			open(sql);
			
			if(id==null) {
				getPreparedStatmant().setNull(1, Types.NULL);
			}else {
				getPreparedStatmant().setLong(1, id);
			}
			
			setResultSet(getPreparedStatmant().executeQuery());
			ResultSet rs = getResultSet();
			
			if(rs.next()) {
				consulta=obterConsulta(rs);
			}
			
			return consulta;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.consulta.obter"));
		} finally {
			
		}
	}

	private Consulta obterConsulta(ResultSet rs) throws SQLException {
		Paciente paciente = new Paciente();
		paciente.setId(rs.getLong("id_paciente"));
		paciente.setNome(rs.getString("nome"));
		paciente.setCpf(rs.getString("cpf"));
		paciente.setDataNascimento(rs.getDate("dataNascimento").toLocalDate());
		paciente.setSexo(rs.getString("sexo"));
		paciente.setTelefone(rs.getString("telefone"));
		
		Medico medico = new Medico();
		medico.setId(rs.getLong("id_medico"));
		medico.setCrm(rs.getString("crm"));
		medico.setNome(rs.getString("nome"));
		medico.setEspecialidade(rs.getString("especialidade"));
		
		Consulta con=new Consulta();
		con.setId(rs.getLong("id_consulta"));
		con.setInformaoes(rs.getString("informacoes"));
		con.setDataConsulta(rs.getDate("dataConsulta").toLocalDate());
		con.setAtendido(rs.getBoolean("atendido"));
		con.setMedico(medico);
		con.setPaciente(paciente);
		return con;
	}

	@Override
	public List<Consulta> listar(Consulta filtro) throws DBException {
		List<Consulta> consultas=new ArrayList<Consulta>();
		StringBuilder sql=new StringBuilder();
		sql.append(" SELECT con.id id_consulta, con.dataConsulta, con.informacoes, con.atendido,")
		   .append(" med.id id_medico, med.crm, med.nome, med.especialidade,")
		   .append(" pac.id id_paciente, pac.nome, pac.cpf, pac.dataNascimento, pac.sexo, pac.telefone")
		   .append(" FROM  Consulta con ")
		   .append(" INNER JOIN Medico med ON con.medico_id = med.id ")
		   .append(" INNER JOIN Paciente pac ON con.paciente_id = pac.id ")
		   .append(" WHERE 1 = 1 ");
		
		List<Object> paramsValue = new ArrayList<Object>();
		
		if(filtro!=null) {
			if(filtro.getId()!=null) {
				sql.append(" AND con.id = ? ");
				paramsValue.add(filtro.getId());
			}
			if(filtro.getDataConsulta()!=null) {
				sql.append(" AND con.dataConsulta = ? ");
				paramsValue.add(filtro.getDataConsulta());
			}
			if(filtro.getMedico()!=null && filtro.getMedico().getId()!=null) {
				sql.append(" AND con.id = ? ");
				paramsValue.add(filtro.getMedico().getId());
			}
			if(filtro.getPaciente()!=null && filtro.getPaciente().getId()!=null) {
				sql.append(" AND con.paciente = ? ");
				paramsValue.add(filtro.getPaciente().getId());
			}
			if(filtro.getInformaoes()!=null) {
				sql.append(" AND con.informacoes = ? ");
				paramsValue.add(filtro.getInformaoes());
			}
			if(filtro.getAtendido()!=null) {
				sql.append(" AND con.atendido = ? ");
				paramsValue.add(filtro.getAtendido());
			}
		}
		
		
		try {
			open(sql);
			
			for (int i = 0; i < paramsValue.size(); i++) {
				getPreparedStatmant().setObject(i+1, paramsValue.get(i));

			}
			
			setResultSet(getPreparedStatmant().executeQuery());
			
			ResultSet rs = getResultSet();
			
			while(getResultSet().next()) {
				consultas.add(obterConsulta(rs));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(new Message("db.erro.consulta.listar"));
		} finally {
			close();
		}
		return consultas;
	}

}

