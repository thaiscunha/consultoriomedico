package br.com.consultorioMedico.test.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.consultorioMedico.ManegerTest;
import br.com.consutorioMedico.dao.EnderecoDaoImpl;
import br.com.consutorioMedico.dao.PacienteDaoImpl;
import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.modelo.Endereco;
import br.com.consutorioMedico.modelo.Paciente;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EnderecoTestDao extends ManegerTest{
	EnderecoDaoImpl dao;
	PacienteDaoImpl pacienteDao;
	static Endereco endereco;

	@Before
	public void setUp() throws Exception {
		configJNDIDataSource();
		dao = new EnderecoDaoImpl();
		pacienteDao = new PacienteDaoImpl();
		
		if(endereco==null) {
			Paciente paciente=new Paciente();
			paciente.setNome("Thais Cunha F Cardoso");
			paciente.setCpf("10958506795");
			paciente.setDataNascimento(LocalDate.of(1986, 05, 20));
			paciente.setSexo("Feminino");
			paciente.setTelefone("(21)987335582");
			pacienteDao.incluir(paciente);
			
			endereco = new Endereco();
			endereco.setLogradouro("Rua Domingos dos Santos");
			endereco.setBairro("Bento Ribeiro");
			endereco.setEstado("Rio de Janeiro");
			endereco.setComplemento("Sobrado");
			endereco.setCep("21.550.580");
			endereco.setPaciente(paciente);
			
		}
		
	}


	@Test
	public void test01_incluir()throws DBException {
		boolean ok=false;
		ok=dao.incluir(endereco);
		assertTrue(ok);
	}
	
	@Test
	public void test02_obter()throws DBException {
		Endereco end = dao.obter(endereco.getId());
		assertNotNull(end);
		
	}
	
	@Test
	public void test03_alterar()throws DBException {
	
		endereco = dao.obter(endereco.getId());
		endereco.setComplemento("SB");
		endereco.setBairro("Prefeito Bento Ribeiro");
		endereco.setEstado("RJ");
		
		boolean ok = dao.alterar(endereco);
		assertTrue(ok);
	}
	@Test
	public void test04_Listar() throws DBException {
		Endereco filtro = new Endereco();
		filtro.setId(endereco.getId());
		List<Endereco> enderecos = dao.listar(filtro);
		//Se nao for vazio est� correto
		assertTrue(!enderecos.isEmpty());
	}
	@Test
	public void test05_Excluir() throws DBException {
		boolean ok = false;
		ok = dao.excluir(endereco);
		pacienteDao.excluir(endereco.getPaciente());
		assertTrue(ok);
	}
	
	

}
