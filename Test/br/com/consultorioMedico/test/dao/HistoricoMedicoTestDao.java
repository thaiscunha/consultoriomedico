package br.com.consultorioMedico.test.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.consultorioMedico.ManegerTest;
import br.com.consutorioMedico.dao.HistoricoMedicoDaoImpl;
import br.com.consutorioMedico.dao.PacienteDaoImpl;
import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.modelo.HistoricoMedico;
import br.com.consutorioMedico.modelo.Paciente;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HistoricoMedicoTestDao extends ManegerTest{
	HistoricoMedicoDaoImpl dao;
	static HistoricoMedico historicoMedico;
	PacienteDaoImpl pacienteDao;

	@Before
	public void setUp() throws Exception {
		configJNDIDataSource();
		dao=new HistoricoMedicoDaoImpl();
		pacienteDao=new PacienteDaoImpl();
		if(historicoMedico==null) {
			Paciente paciente=new Paciente();
			paciente.setNome("Thais Cunha F Cardoso");
			paciente.setCpf("10958506795");
			paciente.setDataNascimento(LocalDate.of(1986, 05, 20));
			paciente.setSexo("Feminino");
			paciente.setTelefone("(21)987335582");
			pacienteDao.incluir(paciente);
			
		    historicoMedico = new HistoricoMedico();
			historicoMedico.setMedicamento("Clavulin");
			historicoMedico.setResultadoUltimosExames("Inflamação periodontal");
			historicoMedico.setTipoDeDoenca("Canal do dente 26");
			historicoMedico.setPaciente(paciente);
			
		}
		
	}


	@Test
	public void test01_incluir()throws DBException {
		boolean ok=false;
		ok=dao.incluir(historicoMedico);
		assertTrue(ok);
	}
	
	@Test
	public void test02_obter()throws DBException {
		HistoricoMedico his = dao.obter(historicoMedico.getId());
		assertNotNull(his);
	}
	
	@Test
	public void test03_alterar()throws DBException{
		historicoMedico.setMedicamento("Suspenso o uso de medicamento");
		
		boolean ok=dao.alterar(historicoMedico);
		assertTrue(ok);
	}
	
	@Test
	public void test04_listar()throws DBException{
		HistoricoMedico filtro=new HistoricoMedico();
		filtro.setId(historicoMedico.getId());
		List<HistoricoMedico >historicos=dao.listar(filtro);
		assertTrue(!historicos.isEmpty());
	}
	
	@Test
	public void test05_excluir()throws DBException{
		boolean ok=false;
		ok=dao.excluir(historicoMedico);
		pacienteDao.excluir(historicoMedico.getPaciente());
		assertTrue(ok);
	}
}
