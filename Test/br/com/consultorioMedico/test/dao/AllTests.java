package br.com.consultorioMedico.test.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ EnderecoTestDao.class, HistoricoMedicoTestDao.class, MedicoTestDao.class,
		PacienteTestDao.class, PagamentoTestDao.class, TipoPagamentoTestDao.class, ConsultaTestDao.class })
public class AllTests {

}
