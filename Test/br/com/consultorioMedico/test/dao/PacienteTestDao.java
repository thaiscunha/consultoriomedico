package br.com.consultorioMedico.test.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.consultorioMedico.ManegerTest;
import br.com.consutorioMedico.dao.EnderecoDaoImpl;
import br.com.consutorioMedico.dao.HistoricoMedicoDaoImpl;
import br.com.consutorioMedico.dao.PacienteDaoImpl;
import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.modelo.Endereco;
import br.com.consutorioMedico.modelo.HistoricoMedico;
import br.com.consutorioMedico.modelo.Paciente;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PacienteTestDao extends ManegerTest {
	PacienteDaoImpl dao;
	static Paciente paciente;
	EnderecoDaoImpl enderecoDao;
	HistoricoMedicoDaoImpl historicoDao;

	@Before
	public void setUp() throws Exception {
		configJNDIDataSource();
		dao = new PacienteDaoImpl();
		enderecoDao = new EnderecoDaoImpl();
		historicoDao = new HistoricoMedicoDaoImpl();
		if (paciente == null) {
			paciente = new Paciente();
			paciente.setNome("Thais Cunha F Cardoso");
			paciente.setCpf("10958506795");
			paciente.setDataNascimento(LocalDate.of(1986, 05, 20));
			paciente.setSexo("Feminino");
			paciente.setTelefone("(21)987335582");

		}

	}

	@Test
	public void test01_incluir() throws DBException {
		boolean ok = false;
		ok = dao.incluir(paciente);
		assertTrue(ok);
	}

	//@Test
	public void test02_incluirEndereco() throws DBException {
		Endereco enderecoPaciente = new Endereco();
		enderecoPaciente.setLogradouro("Rua Domingos dos Santos");
		enderecoPaciente.setBairro("Bento Ribeiro");
		enderecoPaciente.setEstado("Rio de Janeiro");
		enderecoPaciente.setComplemento("Sobrado");
		enderecoPaciente.setCep("21550580");
		enderecoPaciente.setPaciente(paciente);

		boolean ok = false;
		ok = enderecoDao.incluir(enderecoPaciente);
		paciente.setEndereco(enderecoPaciente);
		assertTrue(ok);
	}

	//@Test
	public void test03_incluirHistorico() throws DBException {
		HistoricoMedico historicoMedico = new HistoricoMedico();
		historicoMedico.setTipoDeDoenca("Canal do dente 26");
		historicoMedico.setMedicamento("Clavulin");
		historicoMedico.setResultadoUltimosExames("Inflama��o periodontal.");
		historicoMedico.setPaciente(paciente);

		boolean ok = false;
		ok = historicoDao.incluir(historicoMedico);
		paciente.setHistoricoMedico(historicoMedico);
		assertTrue(ok);
	}
    
	@Test
	public void test04_obterPaciente() throws DBException {
		Paciente e = dao.obter(paciente.getId());
		assertNotNull(e);
	}
	
	@Test
	public void test05_alterarPaciente() throws DBException {
		paciente = dao.obter(paciente.getId());
		paciente.setNome("Tha�s Cunha Ferreira Cardoso");
		
		boolean ok = dao.alterar(paciente);
		assertTrue(ok);
	}
	
	@Test
	public void test06_listarPaciente() throws DBException {
		Paciente filtro=new Paciente();
		filtro.setId(paciente.getId());
		List<Paciente> pacientes=dao.listar(filtro);
		assertTrue(!pacientes.isEmpty());
	}
	
	@Test
	public void test07_excluirPaciente() throws DBException {
		paciente=dao.obter(paciente.getId());
		boolean ok=dao.excluir(paciente);
		assertTrue(ok);
		//O test n�o roda pq existe fk de id paciente dentro de Endereco,historico,consultorio?
	}
	
	

}
