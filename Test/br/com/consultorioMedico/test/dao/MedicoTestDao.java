package br.com.consultorioMedico.test.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.consultorioMedico.ManegerTest;
import br.com.consutorioMedico.dao.MedicoDaoImpl;
import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.modelo.Medico;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MedicoTestDao extends ManegerTest{
	MedicoDaoImpl dao;
	static Medico medico;

	@Before
	public void setUp() throws Exception {
		configJNDIDataSource();
		dao=new MedicoDaoImpl();
		if(medico==null) {
			medico=new Medico();
			medico.setCrm("123.456");
			medico.setNome("Dr Jo�o Paulo");
			medico.setEspecialidade("Endodentista");
			
		}
		
	}


	@Test
	public void test01_incluir()throws DBException {
		boolean ok=false;
		ok=dao.incluir(medico);
		assertTrue(ok);
	}
	
	@Test
	public void test02_alterar()throws DBException {
		medico.setNome("Doutor Jo�o Paulo");
		
		boolean ok=false;
		ok=dao.alterar(medico);
		assertTrue(ok);
	}
	
	@Test
	public void test03_obter()throws DBException {
		Medico m=dao.obter(medico.getId());
		assertNotNull(m);
	}
	
	@Test
	public void test04_listar()throws DBException {
		Medico filtro = new Medico();
		filtro.setId(medico.getId());
		List<Medico> medicos=dao.listar(filtro);
		//Caso n�o esteje vazio
		assertTrue(!medicos.isEmpty());
	}
	
	@Test
	public void test05_excluir()throws DBException {
		boolean ok=false;
		ok=dao.excluir(medico);
		assertTrue(ok);
		
	}

}
