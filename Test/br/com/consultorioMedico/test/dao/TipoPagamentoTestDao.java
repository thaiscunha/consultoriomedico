package br.com.consultorioMedico.test.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.consultorioMedico.ManegerTest;
import br.com.consutorioMedico.dao.TipoPagamentoDaoImpl;
import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.modelo.TipoPagamento;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TipoPagamentoTestDao extends ManegerTest {
	static TipoPagamento tipoPagamento;
	TipoPagamentoDaoImpl dao;

	@Before
	public void setUp() throws Exception {
		configJNDIDataSource();
		dao = new TipoPagamentoDaoImpl();
		if (tipoPagamento == null) {
			tipoPagamento = new TipoPagamento();
			tipoPagamento.setDescricao("Valor do tratamento de canal.");
			tipoPagamento.setValorReferencia(1700.00);
		}

	}
	
	@Test
	public void tes01_incluir() throws DBException {
		boolean ok=false;
		ok=dao.incluir(tipoPagamento);
		assertTrue(ok);
	}
	
	@Test
	public void tes02_obter() throws DBException {
		TipoPagamento tipopg=dao.obter(tipoPagamento.getId());
		assertNotNull(tipopg);
	}
	
	@Test
	public void test03_alterar() throws DBException {
		tipoPagamento = dao.obter(tipoPagamento.getId());
		tipoPagamento.setDescricao("Reembolso plano de sa�de, 70% da tabela.");
		
		boolean ok = dao.alterar(tipoPagamento);
		assertTrue(ok);
	}
	
	@Test
	public void test04_listar() throws DBException {
		TipoPagamento filtro=new TipoPagamento();
		filtro.setId(tipoPagamento.getId());
		List<TipoPagamento> tiposPagamento=dao.listar(filtro);
		assertTrue(!tiposPagamento.isEmpty());
	}
	
	@Test
	public void test05_excluir() throws DBException {
		tipoPagamento=dao.obter(tipoPagamento.getId());
		boolean ok=dao.excluir(tipoPagamento);
		assertTrue(ok);
		
	}


}
