package br.com.consultorioMedico.test.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.consultorioMedico.ManegerTest;
import br.com.consutorioMedico.dao.ConsultaDaoImpl;
import br.com.consutorioMedico.dao.MedicoDaoImpl;
import br.com.consutorioMedico.dao.PacienteDaoImpl;
import br.com.consutorioMedico.dao.PagamentoDaoImpl;
import br.com.consutorioMedico.dao.TipoPagamentoDaoImpl;
import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.modelo.Consulta;
import br.com.consutorioMedico.modelo.Medico;
import br.com.consutorioMedico.modelo.Paciente;
import br.com.consutorioMedico.modelo.Pagamento;
import br.com.consutorioMedico.modelo.TipoPagamento;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PagamentoTestDao extends ManegerTest{
	static Pagamento pagamento;
	PagamentoDaoImpl dao;
	TipoPagamentoDaoImpl daoTipoPagamento;
	MedicoDaoImpl daoMedico;
	PacienteDaoImpl daoPaciente;
	ConsultaDaoImpl daoConsulta;

	@Before
	public void setUp() throws Exception {
		configJNDIDataSource();
		dao = new PagamentoDaoImpl();
		daoConsulta=new ConsultaDaoImpl();
		daoMedico=new MedicoDaoImpl();
		daoPaciente =new PacienteDaoImpl();
		daoTipoPagamento=new TipoPagamentoDaoImpl();
		if (pagamento == null) {
			Medico medico=new Medico();
			medico.setCrm("123.456");
			medico.setNome("Dr Jo�o Paulo");
			medico.setEspecialidade("Endodentista");
			daoMedico.incluir(medico);
			
			Paciente paciente = new Paciente();
			paciente.setNome("Thais Cunha F Cardoso");
			paciente.setCpf("10958506795");
			paciente.setDataNascimento(LocalDate.of(1986, 05, 20));
			paciente.setSexo("Feminino");
			paciente.setTelefone("(21)987335582");
			daoPaciente.incluir(paciente);
			
			Consulta consulta=new Consulta();
			consulta.setDataConsulta(LocalDate.of(2015, 05, 20));
			consulta.setInformaoes("Realizado raio-x para plano de sa�de.");
			consulta.setAtendido(true);
			consulta.setMedico(medico);
			consulta.setPaciente(paciente);
			daoConsulta.incluir(consulta);
			
			TipoPagamento tipoPagamento = new TipoPagamento();
			tipoPagamento.setDescricao("Valor do tratamento de canal.");
			tipoPagamento.setValorReferencia(1700.00);
			daoTipoPagamento.incluir(tipoPagamento);
			
			pagamento = new Pagamento();
			pagamento.setCodAutorizacao(1020);
			pagamento.setValor(1700.00);
			pagamento.setTipoPagamento(tipoPagamento);
			pagamento.setConsulta(consulta);
			
			
		}

	}
	
	@Test
	public void tes01_incluir() throws DBException {
		boolean ok = false;
		ok=dao.incluir(pagamento);
		assertTrue(ok);
	}
	
	@Test
	public void tes02_obter() throws DBException {
		Pagamento pg=dao.obter(pagamento.getId());
		assertNotNull(pg);
	}
	
	@Test
	public void test03_alterar() throws DBException {
		pagamento = dao.obter(pagamento.getId());
		pagamento.setValor(535.50);
		
		boolean ok = dao.alterar(pagamento);
		assertTrue(ok);
	}
	
	@Test
	public void test04_lista() throws DBException {
		Pagamento filtro=new Pagamento();
		filtro.setId(pagamento.getId());
		List<Pagamento> pagamentos=dao.listar(filtro);
		assertTrue(!pagamentos.isEmpty());
	}
	
	@Test
	public void test05_excluir() throws DBException {
		pagamento=dao.obter(pagamento.getId());
		boolean ok=dao.excluir(pagamento);
		assertTrue(ok);
		
	}


}
