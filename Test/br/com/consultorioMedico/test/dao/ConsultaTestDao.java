package br.com.consultorioMedico.test.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.consultorioMedico.ManegerTest;
import br.com.consutorioMedico.dao.ConsultaDaoImpl;
import br.com.consutorioMedico.dao.MedicoDaoImpl;
import br.com.consutorioMedico.dao.PacienteDaoImpl;
import br.com.consutorioMedico.exception.DBException;
import br.com.consutorioMedico.modelo.Consulta;
import br.com.consutorioMedico.modelo.Medico;
import br.com.consutorioMedico.modelo.Paciente;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultaTestDao extends ManegerTest {
	static Consulta consulta;
	MedicoDaoImpl daoMedico;
	PacienteDaoImpl daoPaciente;
	ConsultaDaoImpl dao;

	@Before
	public void setUp() throws Exception {
		configJNDIDataSource();
		dao = new ConsultaDaoImpl();
		daoMedico = new MedicoDaoImpl();
		daoPaciente = new PacienteDaoImpl();
		if (consulta == null) {
			Medico medico = new Medico();
			medico.setCrm("123.456");
			medico.setNome("Dr Jo�o Paulo");
			medico.setEspecialidade("Endodentista");
			daoMedico.incluir(medico);

			Paciente  paciente = new Paciente();
			paciente.setNome("Thais Cunha F. Cardoso");
			paciente.setCpf("10958506795");
			paciente.setDataNascimento(LocalDate.of(1986,05,20));
			paciente.setSexo("Feminino");
			paciente.setTelefone("(21)987335582");
			daoPaciente.incluir(paciente);

			consulta = new Consulta();
			consulta.setDataConsulta(LocalDate.of(2015, 05, 20));
			consulta.setInformaoes("Realizado raio-x para plano de sa�de.");
			consulta.setAtendido(true);
			consulta.setMedico(medico);
			consulta.setPaciente(paciente);

		}
	}

	@Test
	public void tes01_incluir() throws DBException {
		boolean ok = false;
		ok = dao.incluir(consulta);
		assertTrue(ok);
	}
	
	@Test
	public void tes02_obter() throws DBException {
		Consulta cons=dao.obter(consulta.getId());
		assertNotNull(cons);
	}
	
	@Test
	public void test03_alterar() throws DBException {
		consulta = dao.obter(consulta.getId());
		consulta.setInformaoes("80% do tratamento foi conclu�do.");
		
		boolean ok = dao.alterar(consulta);
		assertTrue(ok);
	}
	
	@Test
	public void test04_listar() throws DBException {
		Consulta filtro=new Consulta();
		filtro.setId(consulta.getId());
		filtro.setAtendido(Boolean.TRUE);
		List<Consulta> consultas = dao.listar(filtro);
		assertTrue(!consultas.isEmpty());
	}
	
	@Test
	public void test05_excluir() throws DBException {
		consulta=dao.obter(consulta.getId());
		boolean ok=dao.excluir(consulta);
		assertTrue(ok);
		
	}


}
