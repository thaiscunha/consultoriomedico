package br.com.consultorioMedico;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ManegerTest {
	private static InitialContext ic = null;

	public void configJNDIDataSource() {
		if (ic == null)
			return;

		try {

			System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
			System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");
			ic = new InitialContext();

			ic.createSubcontext("java:");
			ic.createSubcontext("java:comp");
			ic.createSubcontext("java:comp/env");
			ic.createSubcontext("java:comp/env/jdbc");

			MysqlDataSource pool = new MysqlDataSource();
			pool.setURL("jdbc:mysql://localhost:3306/consultorio");
			pool.setUser("root");
			pool.setPassword("123456");
			ic.bind("java:comp/env/jdbc/consultorio", pool);

		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	public void unbind() {

		try {
			InitialContext ic = new InitialContext();
			ic.unbind("java:comp/env/jdbc/consultorio");
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

}
